console.log(document);


// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(txtLastName);
console.log(spanFullName);

txtFirstName.addEventListener('keyup', printFirstName)

function printFirstName (event) {
	spanFullName.innerHTML = txtFirstName.value
}


txtFirstName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})


const labelFirstName = document.querySelector("#label-txt-name");

labelFirstName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked the First Name Label.")
})


// activity -last name

txtLastName.addEventListener('keyup', printLastName)

function printLastName (event) {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}


txtLastName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})


const labelLastName = document.querySelector("#label-txt-lname");


labelLastName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked the Last Name Label.")
})
